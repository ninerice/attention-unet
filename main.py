#!/usr/bin/env python3
import argparse
import datetime
import os
import pathlib
import random
import sys
import torch
from torch.backends import cudnn

from solver import Solver
from data_loader import get_loader

def main(config):
    cudnn.benchmark = True
    if config.mode == 'test':
        torch.backends.cudnn.enabled = False

    # Create directories if not exist
    config.result_path = os.path.join(config.result_path, config.model_id)
    if not os.path.exists(config.result_path):
        pathlib.Path(config.result_path).mkdir(parents=True, exist_ok=True)
    if not os.path.exists(config.model_path):
        pathlib.Path(config.model_path).mkdir(parents=True, exist_ok=True)
    if not os.path.exists(config.result_path):
        pathlib.Path(config.result_path).mkdir(parents=True, exist_ok=True)

    print(config)

    train_loader = get_loader(image_path=config.train_path,
                              image_size=config.image_size,
                              batch_size=config.batch_size,
                              num_workers=config.num_workers,
                              mode='train',
                              extension=config.extension,
                              augmentation_prob=config.augmentation_prob,
                              crop=config.crop)
    valid_loader = get_loader(image_path=config.valid_path,
                              image_size=config.image_size,
                              batch_size=config.batch_size,
                              num_workers=config.num_workers,
                              mode='valid',
                              extension=config.extension,
                              augmentation_prob=0.,
                              crop=config.crop)
    test_loader = get_loader(image_path=config.test_path,
                             image_size=config.image_size,
                             batch_size=1,
                             num_workers=config.num_workers,
                             mode='test',
                             extension=config.extension,
                             augmentation_prob=0.,
                             crop=config.crop)

    solver = Solver(config, train_loader, valid_loader, test_loader)

    
    # Train and sample the images
    if config.mode == 'train':
        solver.train()
    elif config.mode == 'test':
        solver.test()

def required_length(nmin,nmax):
    class RequiredLength(argparse.Action):
        def __call__(self, parser, args, values, option_string=None):
            if not nmin<=len(values)<=nmax:
                msg='argument "{f}" requires between {nmin} and {nmax} arguments'.format(
                    f=self.dest,nmin=nmin,nmax=nmax)
                raise argparse.ArgumentTypeError(msg)
            setattr(args, self.dest, values)
    return RequiredLength

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    
    # model hyper-parameters
    parser.add_argument('--image_size', type=int, default=512)
    parser.add_argument('--t', type=int, default=3, help='t for Recurrent step of R2U_Net or R2AttU_Net')
    
    # training hyper-parameters
    parser.add_argument('--img_ch', type=int, default=3)
    parser.add_argument('--output_ch', type=int, default=1)
    parser.add_argument('--num_epochs', type=int, default=400)
    parser.add_argument('--num_epochs_override', type=int, default=0)
    parser.add_argument('--num_epochs_decay', type=int, default=80)
    parser.add_argument('--num_epochs_decay_override', type=int, default=0)
    parser.add_argument('--batch_size', type=int, default=2)
    parser.add_argument('--num_workers', type=int, default=8)
    parser.add_argument('--lr', type=float, default=0.0001)
    parser.add_argument('--lr_override', type=float, default=0)
    parser.add_argument('--beta1', type=float, default=0.9)        # momentum1 in Adam
    parser.add_argument('--beta2', type=float, default=0.999)      # momentum2 in Adam    
    parser.add_argument('--augmentation_prob', type=float, default=0.99)

    parser.add_argument('--random_seed', type=int, default=0)

    parser.add_argument('--log_step', type=int, default=2)
    parser.add_argument('--val_step', type=int, default=2)

    # misc
    parser.add_argument('--mode', type=str, default='train')
    parser.add_argument('--model_id', type=str, default='')
    parser.add_argument('--resume', type=str, default='')
    parser.add_argument('--resume_info', type=str, default='')
    parser.add_argument('--model_type', type=str, default='U_Net',
                        choices=['U_Net','R2U_Net','AttU_Net','R2AttU_Net', 'ResU_Net'])
    parser.add_argument('--extension', type=str, default='png',
                        choices=['png', 'tiff'])
    parser.add_argument('--model_path', type=str, default='./models')
    parser.add_argument('--common_path', type=str, default='')
    parser.add_argument('--train_path', type=str, default='./dataset/train/')
    parser.add_argument('--valid_path', type=str, default='./dataset/valid/')
    parser.add_argument('--test_path', type=str, default='./dataset/test/')
    parser.add_argument('--result_path', type=str, default='./result/')
    parser.add_argument('--log_path', type=str, default='./logs/')
    parser.add_argument('--max_step', type=int, default=0)
    parser.add_argument('--crop', type=int, nargs='+', default=None, action=required_length(2,4))
    parser.add_argument('--debug_path', type=str, default=None)
    parser.add_argument('--debug_iou', type=float, default=1.0)
    parser.add_argument('--debug_img_limit', type=int, default=100)
    parser.add_argument('--comment', type=str, default="")

    parser.add_argument('--cuda_idx', type=int, default=1)

    config = parser.parse_args()
    need_save = config.mode == 'train'
    if config.resume_info:
         cfg = torch.load(config.resume_info)
         print("=> original command:", " ".join(cfg['argv']))
         print("=>    {}: {}".format(config.resume, cfg['argv']))
         exit(0)

    if config.resume:
        if not os.path.isfile(config.resume):
            f = os.path.join(config.model_path,
                             config.resume)
            if os.path.isfile(f):
                config.resume = f
        if os.path.isfile(config.resume):
            cfg = torch.load(config.resume)
            print("=> resume command from {}: {}".format(config.resume, cfg['argv']))
            config_loaded = parser.parse_args(cfg['argv'][1:])
            config_loaded.resume = config.resume
            config_loaded.debug_path = config.debug_path
            config_loaded.debug_iou = config.debug_iou
            config_loaded.debug_img_limit = config.debug_img_limit
            config_loaded.mode = config.mode
            if config.crop:
                config_loaded.crop = config.crop
                print("override crop:", config.crop)
            else:
                pass

            for o in ["num_epochs", "num_epochs_decay", "lr"]:
                c = getattr(config, o + "_override")
                if c:
                    print("override {} to {}".format(o, c))
                    setattr(config_loaded, o, c)
            config = config_loaded
            resume_path = config.resume
            need_save = False
        else:
            assert False, "cannot find resume file {}".format(config.resume)
    elif config.random_seed:
        print("Use random seed {}".format(config.random_seed))
        random.seed(config.random_seed)
        lr = random.random()*0.0005 + 0.0000005
        augmentation_prob= random.random()*0.7
        epoch = random.choice([100,150,200,250])
        decay_ratio = random.random()*0.8
        decay_epoch = int(epoch*decay_ratio)

        config.augmentation_prob = augmentation_prob
        config.num_epochs = epoch
        config.lr = lr
        config.num_epochs_decay = decay_epoch

    if need_save:
        extra_argv = []
        if not config.model_id:
            config.model_id = '%s-%s-%d-%d-%.4f-%d-%.4f-%d' % (config.model_type,
                                                               config.extension,
                                                               config.image_size,
                                                               config.num_epochs,
                                                               config.lr,
                                                               config.num_epochs_decay,
                                                               config.augmentation_prob,
                                                               config.batch_size)
            config.model_id += '-{date:%Y-%m-%d_%H-%M-%S}'.format(date=datetime.datetime.now())
            if config.comment:
                config.model_id += "_" + config.comment.replace(' ', '-')
            extra_argv = ['--model_id', config.model_id]
        
        resume_path = os.path.join(config.model_path,
                                   config.model_id + '.cmd')
        pathlib.Path(config.model_path).mkdir(parents=True, exist_ok=True)
        torch.save({'argv': sys.argv + extra_argv}, resume_path)

    print("#" * 80)
    if config.mode == 'train':
        print("### Command to resume interrupted training: python {} --resume {}".format(
              sys.argv[0], resume_path))
        print("#" * 80)

    if config.common_path:
        config.train_path = os.path.join(config.common_path, "train")
        config.valid_path = os.path.join(config.common_path, "valid")
        config.test_path = os.path.join(config.common_path, "test")

    main(config)
