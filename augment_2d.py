from skimage.transform import warp, AffineTransform, ProjectiveTransform
from skimage.exposure import equalize_adapthist, equalize_hist, rescale_intensity, adjust_gamma, adjust_log, adjust_sigmoid
from skimage.filters import gaussian
from skimage.util import random_noise
from skimage import io
import random
from skimage import data
from skimage.viewer import ImageViewer
import numpy as np

def randRange(a, b):
    '''
    a utility functio to generate random float values in desired range
    '''
    return random.random() * (b - a) + a

def array_or_single(func):
    def wrapper(*args):
        if len(args) == 0:
            return None
        elif len(args) == 1:
            return func(list(args))[0]
        else:
            return tuple(func(list(args)))

    return wrapper

@array_or_single
def randomAffine(ims):
    '''
    wrapper of Affine transformation with random scale, rotation, shear and translation parameters
    '''
    im = ims[0]
    tform = AffineTransform(scale=(randRange(0.8, 1.25), randRange(0.8, 1.25)),
                            rotation=randRange(-0.15, 0.15),
                            shear=randRange(-0.05, 0.05),
                            translation=(randRange(-im.shape[0]//10, im.shape[0]//10), 
                                         randRange(-im.shape[1]//10, im.shape[1]//10)))
    ret = []
    for im in ims:
        ret.append(warp(im, tform.inverse, mode='constant'))
    
    return ret

def randomPerspective(im):
    '''
    wrapper of Projective (or perspective) transform, from 4 random points selected from 4 corners of the image within a defined region.
    '''
    region = 1/4
    A = np.array([[0, 0], [0, im.shape[0]], [im.shape[1], im.shape[0]], [im.shape[1], 0]])
    B = np.array([[int(randRange(0, im.shape[1] * region)), int(randRange(0, im.shape[0] * region))], 
                  [int(randRange(0, im.shape[1] * region)), int(randRange(im.shape[0] * (1-region), im.shape[0]))], 
                  [int(randRange(im.shape[1] * (1-region), im.shape[1])), int(randRange(im.shape[0] * (1-region), im.shape[0]))], 
                  [int(randRange(im.shape[1] * (1-region), im.shape[1])), int(randRange(0, im.shape[0] * region))], 
                 ])

    pt = ProjectiveTransform()
    pt.estimate(A, B)
    return warp(im, pt, output_shape=im.shape[:2])

@array_or_single
def randomCrop(ims):
    '''
    croping the image in the center from a random margin from the borders
    '''
    margin = 1/10
    ret = []
    for im in ims:
        start = [int(randRange(0, im.shape[0] * margin)),
                 int(randRange(0, im.shape[1] * margin))]
        end = [int(randRange(im.shape[0] * (1-margin), im.shape[0])), 
               int(randRange(im.shape[1] * (1-margin), im.shape[1]))]
        ret.append(im[start[0]:end[0], start[1]:end[1]])
    return ret


def randomIntensity(im):
    '''
    rescales the intesity of the image to random interval of image intensity distribution
    '''
    return rescale_intensity(im,
                             in_range=tuple(np.percentile(im, (randRange(0,10), randRange(90,100)))),
                             out_range=tuple(np.percentile(im, (randRange(0,10), randRange(90,100)))))

def randomGamma(im):
    '''
    Gamma filter for contrast adjustment with random gamma value.
    '''
    return adjust_gamma(im, gamma=randRange(0.9, 1.3))

def randomGaussian(im):
    '''
    Gaussian filter for bluring the image with random variance.
    '''
    return gaussian(im, sigma=randRange(0, 2))
    
def randomFilter(im):
    '''
    randomly selects an exposure filter from histogram equalizers, contrast adjustments, and intensity rescaler and applys it on the input image.
    filters include: equalize_adapthist, equalize_hist, rescale_intensity, adjust_gamma, adjust_log, adjust_sigmoid, gaussian
    '''
    if 0:
        Filters = [equalize_adapthist, equalize_hist, adjust_log, adjust_sigmoid, randomGamma, randomGaussian, randomIntensity]
        filt = random.choice(Filters)
        return filt(im)
    else:
        return im

def randomNoise(im):
    '''
    random gaussian noise with random variance.
    '''
    var = randRange(0.001, 0.01)
    return random_noise(im, var=var)
    
from skimage import data, io
from matplotlib import pyplot as plt

def test_augment(im, Steps=[randomAffine, randomPerspective, randomFilter, randomNoise, randomCrop]):
    '''
    image augmentation by doing a sereis of transfomations on the image.
    '''
    io.imshow(im)
    plt.show()
    for step in Steps:
        im = step(im)
        io.imshow(im)
        plt.show()
    return im

if __name__ == '__main__':
    coins = data.coins()
    test_augment(coins)
