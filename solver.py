import pathlib
import os
import numpy as np
import time
import datetime
import torch
import torchvision
from torch import optim
from torch.autograd import Variable
import torch.nn.functional as F
from network import U_Net,R2U_Net,AttU_Net,R2AttU_Net,ResU_Net,init_weights
import csv
#from tensorboardX import SummaryWriter
from torch.utils.tensorboard import SummaryWriter
from barbar import Bar
from torchvision.utils import save_image
from evaluation import *
from datetime import datetime
import random

class MyBCELoss(torch.nn.Module):
        def __init__(self, device):
              super(MyBCELoss, self).__init__()
              self.device = device

        def forward(self, predict, target):
                assert predict.shape[0] == target.shape[0], "predict & target batch size don't match"
                pp = predict
                pn = -predict
                p = torch.stack((pp, pn), dim=-1)
                t = torch.stack((target, 1 - target), dim=-1)
                target_sum = torch.sum(target) + 1
                target_sum_total = torch.sum(1-target) + torch.sum(target) + 1
                pos_weight = torch.tensor([target_sum_total / target_sum, 1]).to(self.device)
                loss = torch.binary_cross_entropy_with_logits(p, t, pos_weight=pos_weight)
                return loss.mean()

class BinaryDiceLoss(torch.nn.Module):
    """Dice loss of binary class
    Args:
        smooth: A float number to smooth loss, and avoid NaN error, default: 1
        p: Denominator value: \sum{x^p} + \sum{y^p}, default: 2
        predict: A tensor of shape [N, *]
        target: A tensor of shape same with predict
        reduction: Reduction method to apply, return mean over batch if 'mean',
            return sum if 'sum', return a tensor of shape [N,] if 'none'
    Returns:
        Loss tensor according to arg reduction
    Raise:
        Exception if unexpected reduction
    """
    def __init__(self, device, smooth=1, p=2, reduction='mean', threshold=0.5):
        super(BinaryDiceLoss, self).__init__()
        self.smooth = smooth
        self.p = p
        self.reduction = reduction
        self.threshold = threshold
        self.device = device

    def forward(self, predict, target):
        assert predict.shape[0] == target.shape[0], "predict & target batch size don't match"
        predict = torch.sigmoid(predict)
        predict = predict.contiguous().view(predict.shape[0], -1)
        target = target.contiguous().view(target.shape[0], -1)

        if True:
                num = torch.sum(torch.mul(predict, target)) * 2
                den = torch.sum(predict.pow(self.p) + target.pow(self.p))
                loss = 1 - (num + self.smooth) / (den + self.smooth)
        else:
                pp = predict
                pn = -predict
                p = torch.stack((pp, pn), dim=-1)
                t = torch.stack((target, 1 - target), dim=-1)
                target_sum = torch.sum(target) + 1
                target_sum_total = torch.sum(1-target) + torch.sum(target) + 1
                pos_weight = torch.tensor([target_sum_total / target_sum, 1.]).to('cuda')
                loss = torch.binary_cross_entropy_with_logits(p, t, pos_weight=pos_weight)
        if self.reduction == 'mean':
                return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        elif self.reduction == 'none':
            return loss
        else:
            raise Exception('Unexpected reduction {}'.format(self.reduction))

class Solver(object):
        def __init__(self, config, train_loader, valid_loader, test_loader):
                self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

                # Data loader
                self.train_loader = train_loader
                self.valid_loader = valid_loader
                self.test_loader = test_loader

                # Models
                self.unet = None
                self.optimizer = None
                self.img_ch = config.img_ch
                self.image_size = config.image_size
                self.output_ch = config.output_ch
                self.criterion = MyBCELoss(self.device)
                # self.criterion = BinaryDiceLoss(self.device)  # torch.nn.BCELoss()
                self.augmentation_prob = config.augmentation_prob

                # Hyper-parameters
                self.lr = config.lr
                self.beta1 = config.beta1
                self.beta2 = config.beta2

                # Training settings
                self.num_epochs = config.num_epochs
                self.num_epochs_decay = config.num_epochs_decay
                self.batch_size = config.batch_size

                # Step size
                self.log_step = config.log_step
                self.val_step = config.val_step
                self.max_step = config.max_step

                # Path
                self.model_path = config.model_path
                self.result_path = config.result_path
                self.mode = config.mode

                self.model_type = config.model_type
                self.t = config.t
                self.build_model()
                self.log_path = config.log_path
                self.model_id = config.model_id
                self.resume = config.resume
                self.debug_path = config.debug_path
                self.debug_iou = config.debug_iou
                self.debug_img_limit = config.debug_img_limit

                # state
                self.run_state = {
                        'current_epoch': 0,
                        'current_lr': self.lr,
                        'graph_written': 0,
                        'best_score': 0.0,
                        'best_epoch': 0,
                }

        def build_model(self, to_device=True):
                """Build generator and discriminator."""
                if self.model_type =='U_Net':
                        self.unet = U_Net(img_ch=self.img_ch,output_ch=self.output_ch)
                elif self.model_type =='R2U_Net':
                        self.unet = R2U_Net(img_ch=self.img_ch,output_ch=self.output_ch,t=self.t)
                elif self.model_type =='AttU_Net':
                        self.unet = AttU_Net(img_ch=self.img_ch,output_ch=self.output_ch)
                elif self.model_type == 'R2AttU_Net':
                        self.unet = R2AttU_Net(img_ch=self.img_ch,output_ch=self.output_ch,t=self.t)
                elif self.model_type == 'ResU_Net':
                        self.unet = ResU_Net(img_ch=self.img_ch,output_ch=self.output_ch)
                else:
                        assert False, "invalid model_type {}".format(self.model_type)

                self.optimizer = optim.Adam(list(self.unet.parameters()),
                                            self.lr, [self.beta1, self.beta2])
                if to_device:
                        self.model_to_device()

                # self.print_network(self.unet, self.model_type)

        def model_to_device(self):
                self.unet.to(self.device)

        def print_network(self, model, name):
                """Print out the network information."""
                num_params = 0
                for p in model.parameters():
                        num_params += p.numel()
                print(model)
                print(name)
                print("The number of parameters: {}".format(num_params))

        def to_data(self, x):
                """Convert variable to tensor."""
                if torch.cuda.is_available():
                        x = x.cpu()
                return x.data

        def update_lr(self, g_lr, d_lr):
                for param_group in self.optimizer.param_groups:
                        param_group['lr'] = lr

        def reset_grad(self):
                """Zero the gradient buffers."""
                self.unet.zero_grad()

        def compute_accuracy(self,SR,GT):
                SR_flat = SR.view(-1)
                GT_flat = GT.view(-1)

                acc = GT_flat.data.cpu()==(SR_flat.data.cpu()>0.5)

        def tensor2img(self,x):
                img = (x[:,0,:,:]>x[:,1,:,:]).float()
                img = img*255
                return img

        def do_loop(self, writer, data_loader, name, is_training, max_step=0, lr=0, epoch=None):
                case_result_iou = {}
                self.unet.train(is_training)
                if not is_training:
                        self.unet.eval()
                debug_out = 0
                epoch_loss = 0.
                acc = 0.        # Accuracy
                SE = 0.         # Sensitivity (Recall)
                SP = 0.         # Specificity
                PC = 0.         # Precision
                F1 = 0.         # F1 Score
                JS = 0.         # Jaccard Similarity
                DC = 0.         # Dice Coefficient
                PO = 0.         # predicted positive
                GTPO = 0.        # ground true positive
                IOU = 0.
                length = 0
                step = 0
                start_time = time.time()
                for i, (images_norm, images, GT, names) in enumerate(Bar(data_loader)):
                        if (not self.run_state['graph_written']) and writer:
                                self.unet.to(torch.device('cpu'))
                                writer.add_graph(self.unet, images_norm)
                                writer.flush()
                                self.unet.to(self.device)
                                self.run_state['graph_written'] = True
                                print("graph wirtten")
                        images_norm = images_norm.to(self.device)
                        # GT : Ground Truth
                        GT = GT.to(self.device)
                        # SR : Segmentation Result
                        SR = self.unet(images_norm)
                        if False and self.model_type == 'ResU_Net':
                                bce = F.binary_cross_entropy_with_logits(SR, GT)
                                pred = torch.sigmoid(pred)
                                dice = dice_loss(pred, GT)
                                loss = bce * bce_weight + dice * (1 - bce_weight)
                        else:
                                SR_probs = torch.sigmoid(SR)
                                SR_flat = SR_probs.view(SR_probs.size(0),-1)
                                SR_flat_no_sig = SR.view(SR.size(0),-1)
                                SR = SR_probs
                                if writer and step < 2 and epoch is not None:
                                        prefix = name + '/' + str(step) + "___" + "__".join(names)
                                        _, ext = os.path.splitext(names[0])
                                        writer.add_image(prefix + '-image', torchvision.utils.make_grid(images), epoch)
                                        writer.add_image(prefix + '-label', torchvision.utils.make_grid(GT), epoch)
                                        writer.add_image(prefix + '-predict', torchvision.utils.make_grid(SR), epoch)
                                        writer.flush()

                                GT_flat = GT.view(GT.size(0),-1)
                                loss = self.criterion(SR_flat_no_sig, GT_flat)
                                epoch_loss += loss.item()
                                iou = get_IOU(SR, GT)
                                IOU += iou
                        GTA = GT > 0.5
                        for i in range(len(names)):
                                basef = os.path.basename(names[i])
                                basef = basef.split('__')
                                if basef:
                                        basef = basef[0]
                                else:
                                        continue
                                gta = torch.sum(GTA[i])
                                gtas = iou * gta
                                if basef in case_result_iou:
                                        a = case_result_iou[basef]
                                        if gta:
                                                a[0] += gtas
                                                a[1] += gta
                                else:
                                        if gta:
                                                case_result_iou[basef] = [gtas, gta]

                        if self.debug_path and (debug_out < self.debug_img_limit and (iou < self.debug_iou)):
                                pre = os.path.join(self.debug_path, self.model_id)
                                pre = os.path.join(pre, str(epoch) if epoch is not None else "test", name)
                                pathlib.Path(pre).mkdir(parents=True, exist_ok=True)
                                for i in range(len(names)):
                                        n = names[i]
                                        n, _ = os.path.splitext(os.path.basename(n))
                                        p = os.path.dirname(os.path.join(pre, n))
                                        pathlib.Path(p).mkdir(parents=True, exist_ok=True)
                                        img_max = images[i].max()
                                        if False and img_max > 0:
                                                img_w = images[i] / img_max * 255
                                        else:
                                                img_w = images[i]
                                        gt_max = GT[i].max()
                                        if False and gt_max > 0:
                                                gt_w = GT[i] / img_max * 255
                                                sr_w = SR[i] / img_max * 255
                                        else:
                                                gt_w = GT[i]
                                                sr_w = SR[i]
                                        save_image(img_w.float(), os.path.join(pre, n + '-image' + '.png'))
                                        save_image(gt_w, os.path.join(pre, n + '-label' + '.png'))
                                        save_image(sr_w, os.path.join(pre, n + '-predict-' + "%.4f-" % (iou) + '.png'))
                                        debug_out += 1

                        # Backprop + optimize
                        if is_training:
                                self.reset_grad()
                                loss.backward()
                                self.optimizer.step()

                        acc += get_accuracy(SR,GT)
                        SE += get_sensitivity(SR,GT)
                        SP += get_specificity(SR,GT)
                        PC += get_precision(SR,GT)
                        F1 += get_F1(SR,GT)
                        JS += get_JS(SR,GT)
                        DC += get_DC(SR,GT)
                        PO += get_positive_ratio(SR, GT)
                        GTPO += get_GT_positive_ratio(SR, GT)
                        length += images.size(0)
                        step += 1
                        if max_step and step > max_step:
                                print("reach max step {}".format(max_step))
                                break
                        if False and not is_training:
                                p = os.path.join(self.result_path,
                                                 '%s_valid_%d_%d_image.png'%(self.model_type, epoch, step))
                                torchvision.utils.save_image(images.data.cpu(), p)

                acc = acc/step
                SE = SE/step
                SP = SP/step
                PC = PC/step
                F1 = F1/step
                JS = JS/step
                DC = DC/step
                PO = PO/step
                GTPO = GTPO/step
                IOU = IOU/step
                epoch_loss = epoch_loss/length
                score = JS + DC

                if writer and epoch is not None:
                        writer.add_scalar(name + '/Loss', epoch_loss, epoch)
                        writer.add_scalar(name + '/Accuracy', acc, epoch)
                        writer.add_scalar(name + '/Sensitivity', SE, epoch)
                        writer.add_scalar(name + '/Specificity', SP, epoch)
                        writer.add_scalar(name + '/Precision', PC, epoch)
                        writer.add_scalar(name + '/IOU', IOU, epoch)
                        if is_training:
                                for tag, value in self.unet.named_parameters():
                                        tag = tag.replace('.', '/')
                                        writer.add_histogram(tag, value.data.cpu().numpy(), epoch)
                                        if self.model_type != 'ResU_Net':
                                                writer.add_histogram(tag+'/grad', value.grad.data.cpu().numpy(), epoch)
                        writer.flush()
                torch.cuda.empty_cache()
                end_time = time.time()
                real_iou = 0
                real_iou_m = 0
                real_IOU = 0
                for case in case_result_iou:
                        k = case_result_iou[case]
                        k0 = float(k[0])
                        k1 = float(k[1])
                        print(case, k0/k1, k0, k1)
                        real_iou += k0
                        real_iou_m += k1
                        real_IOU += k0 / k1
                real_IOU /= len(case_result_iou)
                # Print the log info
                log_line = '[%s/%d/%s %.1fs] LR: %.6f, Loss: %.4f, Acc: %.4f, SE: %.4f, SP: %.4f, PC: %.4f, F1: %.4f, JS: %.4f, DC: %.4f, PO: %.4f, GTPO: %.4f, IOU: %.4f, REAL-IOU: %.4f SCORE: %.4f' % (
                        epoch, self.num_epochs, name, end_time-start_time, lr, \
                        epoch_loss,\
                        acc,SE,SP,PC,F1,JS,DC,PO,GTPO,IOU,real_IOU,score)
                print(log_line)
                with open(self.get_default_unet_log_path(), 'a+', encoding='utf-8') as d:
                        d.write(log_line + '\n')

                return float(score), IOU

        def load_state(self, unet_path=None):
                if not unet_path:
                        unet_path = self.get_default_unet_path()
                if os.path.isfile(unet_path):
                        state = torch.load(unet_path)
                        self.unet.load_state_dict(state['model_state'])
                        self.optimizer.load_state_dict(state['optimizer_state'])
                        self.run_state = state['run_state']
                        print('%s is Successfully Loaded from %s'%(self.model_type, unet_path))
                else:
                        assert False, "missing file {}".format(unet_path)

        def save_state(self, unet_path=None):
                if not unet_path:
                        unet_path = self.get_default_unet_path()
                state = {
                        'model_state': self.unet.state_dict(),
                        'optimizer_state': self.optimizer.state_dict(),
                        'run_state': self.run_state
                }
                torch.save(state, unet_path)

        def get_default_model_path(self):
                if self.resume:
                        base, _ = os.path.splitext(self.resume)
                else:
                        base = os.path.join(self.model_path, self.model_id)
                return base

        def get_default_unet_log_path(self):
                return self.get_default_model_path() + '.log'

        def get_default_unet_path(self):
                return self.get_default_model_path() + '.state'

        def get_default_unet_iou_path(self, iou):
                return self.get_default_model_path() + '_{}.result'.format(iou)

        def train(self):
                random.seed(datetime.now())
                if self.resume:
                        self.load_state()
                else:
                        # init_weights(self.unet)
                        pass

                #====================================== Training ===========================================#
                #===========================================================================================#
                pathlib.Path(self.log_path).mkdir(parents=True, exist_ok=True)
                log_p = os.path.join(self.log_path, self.model_id)
                writer = SummaryWriter(log_p,
                                       flush_secs=120)

                if self.run_state['current_epoch'] != 0:
                        print("resume from epoch {}".format(self.run_state['current_epoch']))
                while self.run_state['current_epoch'] < self.num_epochs:
                        epoch = self.run_state['current_epoch']
                        lr = self.run_state['current_lr']
                        if self.model_type == 'ResU_Net':
                                self.unet.freeze_backbone(epoch < 100)
                                pass
                        unet_score, _ = self.do_loop(writer, self.train_loader, "Training", True, self.max_step, lr, epoch)
                        unet_score, _ = self.do_loop(writer, self.valid_loader, "Validation", False, self.max_step, lr, epoch)

                        # Decay learning rate
                        decay = 2.0 ** (1/ float(self.num_epochs_decay))
                        if (epoch+1) > self.num_epochs_decay:
                                lr /= decay
                                for param_group in self.optimizer.param_groups:
                                        param_group['lr'] = lr
                                        print ('Decay learning rate to lr: {}.'.format(lr))
                                self.run_state['current_lr'] = lr

                        # Save Best U-Net model
                        if unet_score > self.run_state['best_score']:
                                print('Best %s model score : %.4f'%(self.model_type,
                                                                    unet_score))
                                self.run_state['best_score'] = unet_score
                                self.run_state['best_epoch'] = epoch
                        self.run_state['current_epoch'] += 1
                        self.save_state()
                del self.unet
                torch.cuda.empty_cache()
                print("Done training.")
                self.test()

        def test(self):
                #===================================== Test ====================================#
                torch.cuda.empty_cache()
                self.build_model()
                self.load_state()

                unet_score, iou = self.do_loop(None, self.test_loader, "Test", False, self.max_step, 0, None)
                print("score: ", unet_score)
                with open(self.get_default_unet_iou_path(iou), 'a+') as d:
                        d.write("score: {}, iou: {}\n".format(unet_score, iou))
                        pass
                #f = open(os.path.join(self.result_path,'result.csv'), 'a', encoding='utf-8', newline='')
                #wr = csv.writer(f)
                #wr.writerow([self.model_type,acc,SE,SP,PC,F1,JS,DC,self.lr,best_epoch,self.num_epochs,self.num_epochs_decay,self.augmentation_prob])
                #f.close()
                        

                        
