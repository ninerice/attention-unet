import os
import random
from random import shuffle
import numpy as np
import torch
from torch.utils import data
from torchvision import transforms as T
from torchvision.transforms import functional as F
from PIL import Image
from evaluation import *
from skimage import transform as sktransform
from skimage import io as skio
from skimage import exposure as skexpose
import skimage
import augment_2d as augment

#import imgaug as ia
#import imgaug.augmenters as iaa
#from imgaug.augmentables.segmaps import SegmentationMapsOnImage

def randRange(a, b):
        return random.random() * (b - a) + a

class ImageFolder(data.Dataset):
        def __init__(self, root,image_size=224,mode='train',
                     extension='png',
                     augmentation_prob=0.4, crop=None):
                """Initializes image paths and preprocessing module."""
                self.root = os.path.join(root, "image")
                
                # GT : Ground Truth
                self.GT_paths = os.path.join(root, "label")
                l =  [i for i in os.listdir(self.root) if i.endswith(extension)]
                self.image_paths = list(map(lambda x: os.path.join(self.root, x),l))
                self.image_size = image_size
                self.mode = mode
                self.RotationDegree = [0,90,180,270]
                self.augmentation_prob = augmentation_prob
                self.crop = crop
                print("image count in {} path :{}".format(self.mode,len(self.image_paths)))

        def get_crop_tlhw(self):
                top = self.crop[0]
                left = self.crop[1]
                if len(self.crop) >= 3:
                        h = self.crop[2]
                else:
                        h = self.image_size
                if len(self.crop) >= 4:
                        w = self.crop[3]
                else:
                        w = h
                return top, left, h, w

        def __getitem__(self, index):
                """Reads an image from a file and preprocesses it and returns."""
                image_path = self.image_paths[index]
                # filename = image_path.split('_')[-1][:-len(".jpg")]
                # GT_path = self.GT_paths + 'ISIC_' + filename + '_segmentation.png'
                basename = os.path.basename(image_path)
                basename, ext = os.path.splitext(basename)
                GT_path = os.path.join(self.GT_paths, basename + ".png")
                if not os.path.isfile(GT_path):
                        GT_path = os.path.join(self.GT_paths, basename + "_segmentation.png")
                #print(GT_path)

                if ext in [".tiff"]:
                        return self.getitem_tiff(image_path, GT_path)

                image = Image.open(image_path)
                #print(image_path, image.mode)
                if image.mode == 'L':
                        image = image.convert(mode='RGB')
                        # print(np.array(image.getdata()).reshape(image.size[0], image.size[1], 3).tolist())
                elif image.mode == 'I;16':
                        image = image.convert(mode='F')
                        pass
                GT = Image.open(GT_path)
                # hack, covert GT back to RGB mode to work around a PIL bug
                if GT.mode == 'L':
                        GT = GT.convert(mode='RGB')

                aspect_ratio = image.size[1]/image.size[0]

                Transform = []

                if self.crop:
                        top, left, h, w = self.get_crop_tlhw()
                        image = T.functional.crop(image, top, left, h, w)
                        GT = T.functional.crop(GT, top, left, h, w)

                p_transform = random.random()
                if (self.mode == 'train') and p_transform <= self.augmentation_prob:
                        ResizeRange = random.randint(self.image_size * 7 // 8, self.image_size * 9 // 8)
                        Transform.append(T.Resize((int(ResizeRange*aspect_ratio),ResizeRange)))
                        RotationDegree = random.randint(0,3)
                        RotationDegree = self.RotationDegree[RotationDegree]
                        if (RotationDegree == 90) or (RotationDegree == 270):
                                aspect_ratio = 1/aspect_ratio
                        #filler = 0.0 if image.mode.startswith("F") else 0
                        #num_bands = len(image.getbands())
                        #fill=(filler,) * num_bands
                        Transform.append(T.RandomRotation((RotationDegree,RotationDegree)))
                        RotationRange = random.randint(-10,10)
                        Transform.append(T.RandomRotation((RotationRange,RotationRange)))
                        CropRange = random.randint(self.image_size - 6, self.image_size + 14)
                        Transform.append(T.CenterCrop((int(CropRange*aspect_ratio),CropRange)))
                        Transform = T.Compose(Transform)
                        
                        image = Transform(image)
                        GT = Transform(GT)
                        ShiftRange_left = random.randint(0,20)
                        ShiftRange_upper = random.randint(0,20)
                        ShiftRange_right = image.size[0] - random.randint(0,20)
                        ShiftRange_lower = image.size[1] - random.randint(0,20)
                        image = image.crop(box=(ShiftRange_left,ShiftRange_upper,ShiftRange_right,ShiftRange_lower))
                        GT = GT.crop(box=(ShiftRange_left,ShiftRange_upper,ShiftRange_right,ShiftRange_lower))

                        if random.random() < 0.5:
                                image = F.hflip(image)
                                GT = F.hflip(GT)

                        if random.random() < 0.5:
                                image = F.vflip(image)
                                GT = F.vflip(GT)

                        Transform = T.ColorJitter(brightness=0.2,contrast=0.2,hue=0.02)

                        image = Transform(image)

                        Transform =[]

                height = int(self.image_size*aspect_ratio) - int(self.image_size*aspect_ratio)%16
                height_diff = self.image_size - height
                Transform.append(T.Resize((height, self.image_size)))
                if height_diff != 0:
                        Transform.append(T.Pad((0, height_diff//2, 0, height_diff-height_diff//2)))
                Transform.append(T.ToTensor())
                Transform = T.Compose(Transform)
                
                image = Transform(image)
                GT = Transform(GT)
                # hack, covert GT back to gray mode to work around a PIL bug
                GT = GT[:1,:,:]
                Norm_ = T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)) if image.shape[0] != 1 else T.Normalize((0.5, ), (0.5, ))
                image_norm = Norm_(image)

                return image_norm, image, GT, basename

        def __len__(self):
                """Returns the total number of font files."""
                return len(self.image_paths)

        def getitem_tiff(self, image_path, GT_path):
                image = skio.imread(image_path).astype(np.float32)
                m = (image.max() - image.min()) * 9 / 10
                image = image - image.min()
                if m > 0:
                        image[image > m] = m
                        image = image / m

                GT = skio.imread(GT_path).astype(np.float32)
                m = GT.max()
                if m > 0:
                        GT = GT / m

                # crop as specified in command
                if self.crop:
                        top, left, h, w = self.get_crop_tlhw()
                        image = image[top:top+h, left:left+w]
                        GT = GT[top:top+h, left:left+w]

                aspect_ratio = image.shape[1]/image.shape[0]
                p_transform = random.random()

                if (self.mode == 'train') and p_transform <= self.augmentation_prob:
                        # rotoate 0/90/180/270 degree
                        RotationDegree = random.randint(0,3)
                        RotationDegree = self.RotationDegree[RotationDegree]
                        if (RotationDegree == 90) or (RotationDegree == 270):
                                aspect_ratio = 1/aspect_ratio
                        # print("rotate", RotationDegree)
                        image = sktransform.rotate(image, RotationDegree)
                        GT = sktransform.rotate(GT, RotationDegree)

                        # hflip or not
                        if random.random() < 0.5:
                                np.flip(image, 0)
                                np.flip(GT, 0)

                        # vflip or not
                        if random.random() < 0.5:
                                np.flip(image, 1)
                                np.flip(GT, 1)

                        # randomAffine
                        # image, GT = augment.randomAffine(image, GT)

                        # random Crop
                        # image, GT = augment.randomCrop(image, GT)

                        # other augment
                        # image = augment.randomIntensity(image)
                        # image = augment.randomGamma(image)
                        # image = augment.randomGaussian(image)
                        # image = augment.randomFilter(image)
                        # image = augment.randomNoise(image)
                else:
                        # print("no aug", p_transform, self.augmentation_prob)
                        pass

                # resize and padding
                height = int(self.image_size*aspect_ratio) - int(self.image_size*aspect_ratio)%16
                height_diff = self.image_size - height
                image = sktransform.resize(image, (height, self.image_size))
                GT = sktransform.resize(GT, (height, self.image_size))
                if height_diff != 0:
                        assert False, "add padding"
                        # XXX: todo

                # convert to torch tensor
                image = np.stack((image,)*3, axis=0)
                image = torch.from_numpy(image).float()
                GT = np.stack((GT,)*1, axis=0)
                GT = torch.from_numpy(GT).float()

                # normalize
                Norm_ = T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)) if image.shape[0] != 1 else T.Normalize((0.5, ), (0.5, ))
                image_norm = Norm_(image)

                return image_norm, image, GT, image_path

def get_loader(image_path, image_size, batch_size, num_workers=2,
               mode='train', extension='png', augmentation_prob=0.4, crop=None):
        """Builds and returns Dataloader."""
        
        dataset = ImageFolder(root=image_path, image_size=image_size,
                              mode=mode, extension=extension,
                              augmentation_prob=augmentation_prob,
                              crop=crop)
        data_loader = data.DataLoader(dataset=dataset,
                                      batch_size=batch_size,
                                      shuffle=True,
                                      num_workers=num_workers)
        return data_loader
