# assume the processed data is in ~/processed-data/Data-256-0-1/

# start docker env
./docker_cmds/docker-start-pytorch.bash

# train png, use resnet, the debug file is in ~/debug-png, the model/cmd/log files are all under models directory  
./main.py --crop 192 128 --image_size 256 --model_type ResU_Net --common_path ~/processed-data/Data-256-0-1/attn/ --debug_path ~/debug-png --debug_iou 0.5

# train tiff, use resnet
./main.py --extension tiff --crop 192 128 --image_size 256 --model_type ResU_Net --common_path ~/processed-data/Data-256-0-1/attn/ --debug_path ~/debug-tiff --debug_iou 0.5

# train resume previous training
./main.py --resume models/ResU_Net-tiff-256-400-0.0001-80-0.9900-2-2020-04-25_05-50-12.cmd --debug_path ~/debug-tiff --debug_iou 0.5

# exit docker
exit
