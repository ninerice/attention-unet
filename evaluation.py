import torch
import numpy as np

# SR : Segmentation Result
# GT : Ground Truth

def get_GT_positive_ratio(SR,GT,threshold=0.5):
    #GT = GT == torch.max(GT)
    GT = GT > threshold
    tt = torch.sum(GT)
    tensor_size = GT.nelement()
    return float(tt)/float(tensor_size)

def get_positive_ratio(SR,GT,threshold=0.5):
    SR = SR > threshold
    tt = torch.sum(SR)
    tensor_size = GT.nelement()
    return float(tt)/float(tensor_size)

def get_IOU_acc_sen_spec_pre(SR, GT,threshold=0.5):
    if type(SR).__module__ == np.__name__:
        SR = torch.from_numpy(SR)
    if type(GT).__module__ == np.__name__:
        GT = torch.from_numpy(GT)
    return ([
        get_IOU(SR,GT,threshold),
        get_accuracy(SR,GT,threshold),
        get_sensitivity(SR,GT,threshold),
        get_specificity(SR,GT,threshold),
        get_precision(SR,GT,threshold),
        get_DC(SR,GT,threshold),
        get_positive_ratio(SR,GT,threshold)
    ],
        [" IOU", "ACCU", "SENS", "SPEC", "PREC", "DICE", "POS%"]
    )
def get_accuracy(SR,GT,threshold=0.5):
    # (TP + TN) / 1
    SR = SR > threshold
    GT = GT > threshold #GT == torch.max(GT)
    corr = torch.sum(SR==GT)
    tt = torch.sum(SR)
    tensor_size = SR.nelement()
    acc = float(corr)/float(tensor_size)
    return acc

def get_sensitivity(SR,GT,threshold=0.5):
    # TP / P
    # Sensitivity == Recall
    SR = SR > threshold
    GT = GT > threshold #GT == torch.max(GT)

    # TP : True Positive
    # FN : False Negative
    TP = ((SR==1).float()+(GT==1).float())==2
    FN = ((SR==0).float()+(GT==1).float())==2
    total_positive = float(torch.sum(TP+FN))
    if total_positive == 0:
        return 1
    SE = float(torch.sum(TP))/total_positive
    return SE

def get_specificity(SR,GT,threshold=0.5):
    # TN / N
    SR = SR > threshold
    GT = GT > threshold #GT == torch.max(GT)

    # TN : True Negative
    # FP : False Positive
    TN = ((SR==0).float()+(GT==0).float())==2
    FP = ((SR==1).float()+(GT==0).float())==2

    total_negative = float(torch.sum(TN+FP))
    if total_negative == 0:
        return 1
    SP = float(torch.sum(TN))/total_negative
    return SP

def get_IOU(SR,GT,threshold=0.5):
    # Intersection / Union
    SR = SR > threshold
    GT = GT > threshold #GT == torch.max(GT)

    TP = ((SR==1).float()+(GT==1).float())==2
    a = float(torch.sum(TP))
    b = float(torch.sum(SR))+float(torch.sum(GT))-float(torch.sum(TP))
    if b == 0:
        return 1
    IOU = a/b
    return IOU

def get_precision(SR,GT,threshold=0.5):
    # TP / (TP + FP)
    SR = SR > threshold
    GT = GT > threshold #== torch.max(GT)

    # TP : True Positive
    # FP : False Positive
    TP = ((SR==1).float()+(GT==1).float())==2
    FP = ((SR==1).float()+(GT==0).float())==2
    total_pred = float(torch.sum(TP+FP))
    if total_pred == 0:
        return 1

    PC = float(torch.sum(TP))/total_pred

    return PC

def get_F1(SR,GT,threshold=0.5):
    # Sensitivity == Recall
    SE = get_sensitivity(SR,GT,threshold=threshold)
    PC = get_precision(SR,GT,threshold=threshold)
    total = float(SE+PC)
    if total == 0:
        return 0

    F1 = 2*SE*PC/total

    return F1

def get_JS(SR,GT,threshold=0.5):
    # JS : Jaccard similarity
    SR = (SR > threshold).float()
    GT = (GT > threshold).float()
    
    Inter = torch.sum((SR+GT)==2)
    Union = torch.sum((SR+GT)>=1)
    if Union == 0:
        return 1
    JS = float(Inter)/Union

    return JS

def get_DC(SR,GT,threshold=0.5):
    # U / 1
    # DC : Dice Coefficient
    SR = (SR > threshold).float()
    GT = (GT > threshold).float()

    Inter = torch.sum((SR+GT)==2)
    total = float(torch.sum(SR)+torch.sum(GT))
    if total == 0:
        return 1
    DC = float(2*Inter)/total

    return DC



